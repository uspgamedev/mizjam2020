extends Node2D

export(float) var speed = 2000
export(int) var damage = 5

var direction: Vector2 = Vector2.RIGHT

func _process(delta):
	position += direction * speed * delta

func setup(pos: Vector2, angle: float, flip : bool) -> void:
	position = pos
	rotation_degrees = angle
	$Sprite.flip_v = flip
	direction = direction.rotated(deg2rad(angle))

func _on_Area2D_body_entered(body):
	if(body.has_method("damage")):
		body.damage(damage)
	queue_free()

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
