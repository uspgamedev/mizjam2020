extends Gun_Base

const MAGAZINE = 5

var shots : int = MAGAZINE

signal created_projectile(ammo)

onready var sprite = $Sprite
onready var cooldown = $Cooldown
onready var reload_delay = $Reload_Delay
onready var reload_btn = $Reload_Button
onready var reload_bar = $TextureProgress
onready var tween = $Tween
onready var smoke = $Shoot_smoke
onready var reload = $Reload
onready var chicken_sfx = $Chicken_sound
onready var reload_sfx = $Chicken_reload

export(PackedScene) var ammo_scene

var reloading : bool = false
var flip_ammo : bool = false

func _input(event):
	if event.is_action_pressed("hero_shoot"):
		if reload_btn.visible and shots == 0:
			reload_btn.hide()
			reload()
		else: shoot()

	elif event.is_action_pressed("hero_reload"):
		reload_btn.hide()
		reload()

func _process(delta):
	rotate_gun()
	
	if(get_global_mouse_position().x - self.global_position.x) < 0:
		flip_ammo = true
	else: flip_ammo = false
	
	if shots == 0:
		reload_btn.set_rotation_degrees(-rotation_degrees)
		reload_btn.show()

func shoot():
	if not cooldown.is_stopped() or reloading:
		return

	elif shots > 0:
		var ammo = ammo_scene.instance()
		ammo.setup(sprite.global_position, rotation_degrees, flip_ammo)
		shots -= 1
		emit_signal("created_projectile", ammo)
		smoke.emitting = true
		chicken_sfx.play(0.0)
		tween.interpolate_property(sprite, "position:x", sprite.position.x, 60, 0.1, Tween.TRANS_EXPO, Tween.EASE_OUT)
		tween.start()
		cooldown.start()

func reload():
	shots = MAGAZINE
	reloading = true
	reload.interpolate_property(reload_bar, "value", 0, 100, (reload_delay.wait_time - 0.2), Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	reload.start()
	reload_sfx.play(0.0)
	reload_delay.start()

func _on_Reload_Delay_timeout():
	reloading = false
	reload_bar.value = 0

func _on_Tween_tween_all_completed():
	tween.interpolate_property(sprite, "position:x", sprite.position.x, 70, 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.start()
