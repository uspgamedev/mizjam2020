extends Gun_Base

const damage : int = 5

var aim_variance : Vector2 = Vector2.ZERO

enum {AIMING, UP, DOWN, STABILIZE}
var state : int = AIMING

func _physics_process(delta):
	rotate_gun()

	var mouse_pos : Vector2 = get_global_mouse_position()

	$Aim.global_position = mouse_pos + aim_variance
	$Aim.rotation = -self.rotation

	if (mouse_pos.x - self.global_position.x) < 0:
		$Sprite.scale.y = -1

	else:
		$Sprite.scale.y = 1

func shoot():
	if state == AIMING:
		for body in $Aim/Hitbox.get_overlapping_bodies():
			body.damage(self.damage)

		state = UP
		$Shoot_Sound.play()
		$Aim.modulate.a = 0.3
		$Tween.interpolate_property($Sprite, "rotation_degrees", $Sprite.rotation_degrees, -75,
									0.2, Tween.TRANS_ELASTIC, Tween.EASE_OUT)
		$Tween.interpolate_property(	$Aim, "scale", $Aim.scale, Vector2(3, 3),
									0.2, Tween.TRANS_ELASTIC, Tween.EASE_OUT)
		$Tween.interpolate_property(	self, "aim_variance", self.aim_variance, Vector2(0, -70),
									0.2, Tween.TRANS_ELASTIC, Tween.EASE_OUT)
		$Tween.start()

func _on_Tween_tween_all_completed():
	match(state):
		UP:
			$Tween.interpolate_property($Sprite, "rotation_degrees", $Sprite.rotation_degrees, 30,
									0.8, Tween.TRANS_QUAD, Tween.EASE_IN)
			$Tween.interpolate_property(	$Aim, "modulate:a", $Aim.modulate.a, 0.5,
									0.8, Tween.TRANS_QUAD, Tween.EASE_IN)
			$Tween.interpolate_property(	$Aim, "scale", $Aim.scale, Vector2(2, 2),
									0.8, Tween.TRANS_QUAD, Tween.EASE_IN)
			$Tween.interpolate_property(	self, "aim_variance", self.aim_variance, Vector2(0, 30),
									0.8, Tween.TRANS_QUAD, Tween.EASE_IN)
			$Tween.start()
			state = DOWN

		DOWN:
			$Tween.interpolate_property($Sprite, "rotation_degrees", $Sprite.rotation_degrees, -10,
									0.5, Tween.TRANS_QUAD, Tween.EASE_IN)
			$Tween.interpolate_property(	$Aim, "modulate:a", $Aim.modulate.a, 0.6,
									0.5, Tween.TRANS_QUAD, Tween.EASE_IN)
			$Tween.interpolate_property(	$Aim, "scale", $Aim.scale, Vector2(1.6, 1.6),
									0.5, Tween.TRANS_QUAD, Tween.EASE_IN)
			$Tween.interpolate_property(	self, "aim_variance", self.aim_variance, Vector2(0, -10),
									0.5, Tween.TRANS_QUAD, Tween.EASE_IN)
			$Tween.start()
			state = STABILIZE

		STABILIZE:
			$Tween.interpolate_property($Sprite, "rotation_degrees", $Sprite.rotation_degrees, 0,
									0.1, Tween.TRANS_LINEAR, Tween.EASE_IN)
			$Tween.interpolate_property(	$Aim, "modulate:a", $Aim.modulate.a, 0.7,
									0.1, Tween.TRANS_LINEAR, Tween.EASE_IN)
			$Tween.interpolate_property(	$Aim, "scale", $Aim.scale, Vector2(1.5, 1.5),
									0.1, Tween.TRANS_LINEAR, Tween.EASE_IN)
			$Tween.interpolate_property(	self, "aim_variance", self.aim_variance, Vector2(0, 0),
									0.1, Tween.TRANS_LINEAR, Tween.EASE_IN)
			$Tween.start()
			state = AIMING
