extends Gun_Base

signal created_projectile(car)

var colors = [Color8(199, 1, 1), Color8(4, 68, 203), Color8(199, 183, 13), Color8(23, 125, 2)]
onready var color = 0
var rng = RandomNumberGenerator.new()

var shoot : bool = true

var side: int = 2

export(PackedScene) var car_scene

func _ready():
	rng.randomize()

func _physics_process(delta):
	rotate_gun()

	if (get_global_mouse_position().x - self.global_position.x) < 40 && (get_global_mouse_position().x - self.global_position.x) > -40:
		side = 1
		$Car.frame = 46
		$Car.rotation_degrees = 90
	elif (get_global_mouse_position().x - self.global_position.x) < -40 :
		side = 0
		$Car.frame = 44
		$Car.rotation_degrees = 180
	else:
		side = 2
		$Car.frame = 44
		$Car.rotation_degrees = 0

func shoot():
	if !shoot:
		return
	$Cooldown.start()
	shoot = false
	var car = car_scene.instance()
	car.setup($Car.global_position, rotation_degrees, side, colors[color])
	emit_signal("created_projectile", car)
	color = rng.randi_range(0, 3)
	$Car.modulate = colors[color]
	$Car.modulate.a = 0

func _on_Cooldown_timeout():
	$Tween.interpolate_property($Car, "position", Vector2(9, 0), Vector2(18, 0), 0.5)
	$Tween.interpolate_property($Car, "modulate:a", 0, 1, 0.5)
	$Tween.start()

func _on_Tween_tween_all_completed():
	shoot = true
