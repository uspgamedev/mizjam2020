extends Node2D

var speed : int = 1000
var direction := Vector2(1, 0)
var damage : int = 3

func _ready():
	$Horn.play()

func _process(delta):
	position += speed * delta * direction

func setup(pos:Vector2, angle:float, side, color) -> void:
	position = pos
	rotation_degrees = angle
	direction = direction.rotated(deg2rad(angle))
	$Sprite.modulate = color
	if side == 0:
		$Sprite.frame = 44
		$Sprite.rotation_degrees += 180
	elif side == 1:
		$Sprite.frame = 46
		$Sprite.rotation_degrees = 90
	else:
		$Sprite.frame = 44

func _on_Area2D_body_entered(body):
	if(body.has_method("damage")):
		body.damage(damage)

func _on_VisibilityNotifier2D_screen_exited():
	if !$Horn.is_playing():
		queue_free()
