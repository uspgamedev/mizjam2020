extends Node2D

const dmg_proc : int = 2

const MAX_HEAT : int = 100
const HEAT_SEC : float = 30.0
var heat : float = 20.0

var burning : bool = false

func _ready():
	update_visual()

func _physics_process(delta):
	$Sprite.rotation = -get_parent().rotation

	if not burning:
		heat -= HEAT_SEC*delta
		heat = max(0, heat)
		update_visual()

		if heat == 0:
			self.call_deferred("free")

func update_visual():
	var hpc : float = heat/MAX_HEAT
	hpc = min(hpc, 1)
	get_parent().modulate = Color(1, 1-hpc, 1-hpc)
	$Sprite.modulate.a = 0.7*hpc

func increase_heat():
	heat += 20
	heat = min(heat, MAX_HEAT)

	if not burning:
		update_visual()

		if heat == MAX_HEAT:
			$Burn_Proc.start()
			burning = true

func _on_Burn_Proc_timeout():
	get_parent().damage(dmg_proc)
	heat -= 10
	if heat <= 0:
		self.call_deferred("free")

