extends Gun_Base

const frames = [129, 282, 283, 323, 368, 372, 419, 426, 464, 608]
const colors = [Color(0.6, 0.57, 0.57), Color(0.6, 0.35, 0.16), Color(0.87, 0.9, 0.23)]

export(PackedScene) var thrown_sword

signal created_projectile(sword)

var left : bool = false
var on_cooldown : bool = false

func _ready():
	randomize()
	$Sprite.frame = frames[randi()%frames.size()]
	$Sprite.modulate = colors[randi()%colors.size()]

func _physics_process(delta):
	rotate_gun()

	if (get_global_mouse_position().x - self.global_position.x) < 0:
		$Sprite.rotation_degrees = 125
		left = true
	else:
		$Sprite.rotation_degrees = -70
		left = false


func shoot():
	if not on_cooldown:
		on_cooldown = true
		var sword = thrown_sword.instance()
		sword.setup($Sprite.global_position, self.rotation, $Sprite.frame, $Sprite.modulate, left)
		emit_signal("created_projectile", sword)
		$Sprite.frame = frames[randi()%frames.size()]
		$Sprite.modulate = colors[randi()%colors.size()]
		$Sprite.modulate.a = 0
		$Cooldown.start()

func _on_Cooldown_timeout():
	$Tween.interpolate_property($Sprite, "modulate:a", 0, 1, 0.2)
	$Tween.interpolate_property($Sprite, "scale", Vector2(0.01, 0.01), Vector2.ONE, 0.5, Tween.TRANS_BOUNCE, Tween.EASE_IN_OUT)
	$Tween.start()

func _on_Tween_tween_all_completed():
	on_cooldown = false
