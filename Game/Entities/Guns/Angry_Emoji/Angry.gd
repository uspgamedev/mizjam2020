extends KinematicBody2D

const ACCEL : float = 8000.0
const MAX_SPEED : float = 800.0

var speed : Vector2 = Vector2.ZERO

var gun : Sprite
var returning : bool = false
var damage : int = 2

func _physics_process(delta):
	if not gun.is_inside_tree():
		self.call_deferred("free")
		return

	var dir : Vector2
	if not returning:
		dir =  get_local_mouse_position().normalized()

		if not Input.is_mouse_button_pressed(1):
			returning = true
			$Sprite.frame = 707
			$Vibration.stop()
			$Sprite.position = Vector2.ZERO
			$Sprite.modulate = Color(0.94902, 1, 0, 0.5)
			$Hitbox.call_deferred("set", "monitoring", false)
			self.set_collision_mask_bit(2, false)

	else:
		dir = gun.global_position - self.global_position

		if dir.length() < 50:
			gun.returned()
			self.call_deferred("free")

	speed += dir.normalized()*ACCEL*delta
	speed = speed.clamped(MAX_SPEED)
	# warning-ignore:return_value_discarded
	self.move_and_slide(speed)

func _on_Vibration_timeout():
	$Sprite.position = Vector2(rand_range(-5,5), rand_range(-5, 5))

func _on_Hitbox_body_entered(body):
	body.damage(2)

func _on_tree_exited():
	gun.returned()
