extends Gun_Base

export(PackedScene) var angry : PackedScene

var out : bool = false
signal created_projectile(emoji)

func _on_Angry_Emoji_tree_entered():
	out = false
	$Sprite.show()

func _physics_process(_delta):
	rotate_gun()
	$Sprite.rotation = -self.rotation

func shoot():
	if not out:
		out = true
		$Sprite.hide()
		var angry_proj : KinematicBody2D = angry.instance()
		angry_proj.global_position = $Sprite.global_position
		angry_proj.gun = $Sprite
		emit_signal("created_projectile", angry_proj)

func returned():
	out = false
	$Sprite.show()
