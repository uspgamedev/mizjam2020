extends Node2D

onready var anim_player = $AnimationPlayer

export(int) var damage_value
export(Gradient) var colors : Gradient


func _ready():
	$Sprite.modulate = colors.get_color(randi() % colors.get_point_count())


func _on_Area2D_body_entered(body):
	if body is Enemy:
		(body as Enemy).damage(damage_value)
		if not anim_player.current_animation == "die":
			anim_player.play("die")
