extends Node2D

var direction := Vector2(1, 0)
var speed : int = 400
var damage : int = 5

func _ready():
	$AnimationPlayer.play("explosion")

func _physics_process(delta):
	position += speed * delta * direction

func setup(pos, angle):
	self.position = pos
	rotation_degrees = angle
	direction = direction.rotated(deg2rad(angle))

func _on_AnimationPlayer_animation_finished(explosion):
	$Sprite.modulate.a = 0
	$Hitbox/CollisionShape2D.disabled = false
	$Lifetime.start()
	$CPUParticles2D.emitting = true

func _on_Hitbox_body_entered(body):
	yield($Lifetime, "timeout")
	if(body.has_method("damage")):
		body.damage(damage)

func _on_Sticky_body_entered(body):
	if body != get_parent():
		get_parent().call_deferred("remove_child", get_parent().get_node(self.name))
		body.call_deferred("add_child", self)
		self.position = Vector2.ZERO
		self.rotation = 0
		speed = 0

func _on_Lifetime_timeout():
	queue_free()
