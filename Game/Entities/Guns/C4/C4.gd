extends Gun_Base

signal created_projectile(card)

export (PackedScene) var c4_scene

func _physics_process(delta):
	rotate_gun()

func shoot():
	if !$Cooldown.is_stopped():
		return
	$Cooldown.start()

	var bomb = c4_scene.instance()
	bomb.setup($Sprite.global_position, rotation_degrees)
	emit_signal("created_projectile", bomb)
