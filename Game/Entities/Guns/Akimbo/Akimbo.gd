extends Gun_Base

const rot_spd : float = 2.0

export(PackedScene) var bullet : PackedScene
var shooting : bool = false

var shoot_timer : float = 0.0

signal created_projectile(bullet)

func _physics_process(delta):
	rotate_gun()

	if shooting:
		if $Sprite2.rotation < 1.57:
			var akimbo_rot : float = rot_spd*delta
			$Sprite.rotation -= akimbo_rot
			$Sprite2.rotation += akimbo_rot

		while shoot_timer >= 0.0:
			var up_bullet = bullet.instance()
			up_bullet.global_position = $Sprite/barrel.global_position
			up_bullet.direction = ($Sprite/barrel.global_position - self.global_position).normalized()

			var down_bullet = bullet.instance()
			down_bullet.global_position = $Sprite2/barrel.global_position
			down_bullet.direction = ($Sprite2/barrel.global_position - self.global_position).normalized()

			emit_signal("created_projectile", up_bullet)
			emit_signal("created_projectile", down_bullet)

			shoot_timer -= 0.04

		shoot_timer += delta

	elif $Sprite2.rotation > 0:
		var akimbo_rot : float = -rot_spd*(2)*delta
		$Sprite.rotation -= akimbo_rot
		$Sprite2.rotation += akimbo_rot

func _input(event):
	if event.is_action_pressed("hero_shoot"):
		shooting = true

	elif event.is_action_released("hero_shoot"):
		shooting = false
