extends Node2D

signal returned

onready var anim_player = $AnimationPlayer
onready var path = $Path2D
onready var path_follow = $Path2D/PathFollow2D
onready var tween = $Tween

export var damage_value := 2
export(float) var speed = 0.7

var going_back = false


func _ready():
	var duration = 1.0 / speed
	tween.interpolate_property(path_follow, "unit_offset", 0, 1, duration)
	tween.start()


func setup(pos:Vector2, angle:float):
	$Path2D.curve = $Path2D.curve.duplicate()
	var point = $Path2D.curve.get_point_position(1)
	point = point.rotated(deg2rad(angle)) + pos
	$Path2D.curve.set_point_position(1, point)


func update_points(pos:Vector2):
	path.curve.set_point_position(0, pos)
	path.curve.set_point_position(2, pos)


func go_back():
	going_back = true
	anim_player.play_backwards("spin")
	
	tween.stop_all()
	var duration = path_follow.unit_offset / speed
	tween.interpolate_property(path_follow, "unit_offset", null, 0, duration)
	tween.start()


func _on_Area2D_body_entered(body):
	if body is Enemy:
		(body as Enemy).damage(damage_value)
	elif not going_back:
		go_back()


func _on_Tween_tween_completed(_object, _key):
	emit_signal("returned")
	queue_free()
