extends Gun_Base

signal created_projectile(proj)

onready var sprite = $Sprite

export(PackedScene) var projectile_scene

var flying := false
var projectile = null


func _ready():
	pass


func _physics_process(_delta):
	if flying:
		projectile.update_points(sprite.global_position)

	rotate_gun()


func shoot():
	if flying:
		return

	sprite.hide()
	flying = true
	projectile = projectile_scene.instance()
	projectile.setup(sprite.global_position, rotation_degrees)
	projectile.connect("returned", self, "_on_projectile_returned")
	emit_signal("created_projectile", projectile)


func _on_projectile_returned():
	flying = false
	sprite.show()
