extends Gun_Base

var damage : int = 3

var fish_under : int = 1
var looking_left : bool = false
var attacking : bool = false

const MAX_SIZE : Vector2 = Vector2(2,2)

func _physics_process(_delta):
	if not attacking:
		rotate_gun()

		if (get_global_mouse_position().x - self.global_position.x) < 0:
			if fish_under == 1:
				$Sprite.rotation_degrees = 120
			else:
				$Sprite.rotation_degrees = -30
			looking_left = true
		else:
			if fish_under == 1:
				$Sprite.rotation_degrees = -30
			else:
				$Sprite.rotation_degrees = 120
			looking_left = false

func shoot():
	if not attacking:
		attacking = true
		var fish_rotation : int = 150 * fish_under
		if looking_left: fish_rotation *= -1
		fish_under *= -1
		$Tween.interpolate_property($Sprite, "rotation_degrees",
									$Sprite.rotation_degrees, $Sprite.rotation_degrees+fish_rotation,
									0.1, Tween.TRANS_LINEAR, Tween.EASE_OUT)
		$Tween.start()
		$Hitbox/Area.call_deferred("set", "disabled", false)
		$Cooldown.start()

func _on_Cooldown_timeout():
	attacking = false
	$Hitbox/Area.call_deferred("set", "disabled", true)

func _on_Hitbox_body_entered(body):
	body.damage(self.damage, true)
	if $Sprite.scale < MAX_SIZE:
		$Sprite.scale += Vector2(0.1,0.1)
		$Sprite.position.x += 0.14
		$Hitbox/Area.shape.call_deferred("set", "radius", $Hitbox/Area.shape.radius+0.8)
		$Hitbox/Area.call_deferred("set", "position", $Hitbox/Area.position + Vector2(1,0))
		$Fish_Reset.start()

func _on_Fish_Reset_timeout():
	$Sprite.scale = Vector2(0.7,0.7)
	$Sprite.position.x = 10
	$Hitbox/Area.position.x = 0
	$Hitbox/Area.shape.radius = 10
