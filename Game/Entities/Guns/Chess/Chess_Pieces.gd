extends Node2D

var speed : int = 400
var direction := Vector2(1, 0)
var damage : float = 5
var x : int = 128
onready var stop : bool = false
var piece : String
var n : int

func _ready():
	$Timer.start()
	if $Sprite.frame == 1007:
		$Horse.start()
	if $Sprite.frame == 1005:
		$Queen.start()

func _process(delta):
	if !$Timer.is_stopped():
		position += speed * delta * direction

func setup(pos:Vector2, angle:float, frame, left) -> void:
	$Sprite.frame = frame
	position = pos
	rotation_degrees = angle
	if left:
		$Sprite.flip_v = true
	direction = direction.rotated(deg2rad(angle))
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	n = rng.randi_range(0,1)
	if frame == 1004:
		if n == 1:
			direction = direction.rotated(deg2rad(-45))
		else:
			direction = direction.rotated(deg2rad(45))
	elif frame == 1007:
		piece = "horse"
	elif frame == 1003:
		speed *= 1.5
	elif frame == 1005:
		speed *= 1.5
		damage *= 2
		if n == 1:
			direction = direction.rotated(deg2rad(-45))
		else:
			direction = direction.rotated(deg2rad(45))
	elif frame == 1006:
		damage *= 2

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()

func _on_Hitbox_body_entered(body):
	if(body.has_method("damage")):
		body.damage(damage)
	queue_free()

func _on_Timer_timeout():
	$Tween.interpolate_property(self, "modulate:a", 1, 0, 1, Tween.TRANS_CIRC, Tween.EASE_IN_OUT)
	$Tween.start()

func _on_Tween_tween_all_completed():
	self.queue_free()

func _on_Horse_timeout():
	if n == 1:
		direction = direction.rotated(deg2rad(90))
	else:
		direction = direction.rotated(deg2rad(-90))

func _on_Queen_timeout():
	$Queen.start()
	if n == 1:
		direction = direction.rotated(deg2rad(45))
		n = 0
	else:
		direction = direction.rotated(deg2rad(-45))
		n = 1
