extends Area2D

var dir : Vector2 = Vector2.ZERO

func _ready():
	self.scale = Vector2(rand_range(0.5, 2), rand_range(0.5, 2))
	self.modulate = Color(rand_range(0, 1), rand_range(0, 1), rand_range(0, 1))
	self.dir = self.dir.rotated(rand_range(-1, 1))

func _physics_process(delta):
	self.dir = self.dir.rotated(rand_range(-0.2, 0.2))
	self.position += dir*rand_range(0, 500)*delta

func _on_visible_screen_exited():
	self.call_deferred("free")

func _on_Glitch_Projectile_body_entered(body):
	body.damage(1)
