extends Node2D

var speed : int = 800
var direction := Vector2(1, 0)
var damage

func _process(delta):
	position += speed * delta * direction

func setup(pos:Vector2, angle:float, frame) -> void:
	$Sprite.frame = frame
	damage = (frame - 761)
	position = pos
	rotation_degrees = angle
	direction = direction.rotated(deg2rad(angle))

func _on_Area2D_body_entered(body):
	if(body.has_method("damage")):
		body.damage(damage)
	queue_free()


func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
