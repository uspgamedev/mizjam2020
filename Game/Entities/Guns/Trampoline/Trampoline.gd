extends Gun_Base

var damage : int = 5

func _physics_process(delta):
	rotate_gun()

func shoot():
	if $Cooldown.is_stopped():
		$Cooldown.start()
		$AnimationPlayer.play("attack")
		$Area2D/CollisionShape2D.call_deferred("set", "disabled", false)

func _on_Area2D_body_entered(body):
	if(body.has_method("damage")):
		body.damage(damage, true)

func _on_Cooldown_timeout():
	$Area2D/CollisionShape2D.call_deferred("set", "disabled", true)
