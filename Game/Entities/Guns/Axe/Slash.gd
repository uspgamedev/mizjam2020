extends Node2D

var speed : int = 1000
var direction := Vector2(1, 0)
var damage : float = 4.0

func _process(delta):
	position += speed * delta * direction

func setup(pos:Vector2, angle:float) -> void:
	position = pos
	rotation_degrees = angle
	direction = direction.rotated(deg2rad(angle))
	$AnimationPlayer.play("slash")
	if $Timer.is_inside_tree():
		$Timer.start()
	else:
		$Timer.autostart = true

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()

func _on_Hitbox_body_entered(body):
	if(body.has_method("damage")):
		body.damage(damage*(2 - $Timer.time_left))
