extends Enemy
class_name Base_Eye

signal created_projectile(shoot)

export(PackedScene) var projectile : PackedScene

func _physics_process(_delta):
	$Eye.rotation = (hero.global_position - self.global_position).angle()-1.57

func _on_Shoot_Timer_timeout():
	var new_shoot = projectile.instance()
	new_shoot.dir = (hero.global_position - self.global_position).normalized()
	new_shoot.global_position = self.global_position
	emit_signal("created_projectile", new_shoot)
