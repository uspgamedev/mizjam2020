extends Enemy
class_name Base_Mouth

signal created_projectile(shoot)

export(PackedScene) var monster : PackedScene

func _on_Spawn_Timer_timeout():
	var new_mons : Enemy = monster.instance()
	new_mons.hero = self.hero
	new_mons.global_position = self.global_position
	emit_signal("created_projectile", new_mons)
