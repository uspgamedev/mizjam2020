extends Area2D

var accel : float = 1.0
var dir : Vector2 setget dir_changed

func dir_changed(new_dir : Vector2):
	dir = new_dir.normalized()
	$Sprite.rotation = dir.angle()+0.78

func _physics_process(delta):
	accel += 700*delta
	self.position += dir*delta*accel

func _on_Test_Eye_Projectile_body_entered(body):
	if body is Hero:
		body.damage(self.global_position)

	self.call_deferred("free")
