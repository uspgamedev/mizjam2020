extends Enemy

export(NodePath) var Boss_Node : NodePath

func _physics_process(delta):
	var boss : Enemy = get_node(Boss_Node)
	for piece in boss.get_node("Pieces").get_children():
		piece.hero = self.hero

	self.call_deferred("free")
