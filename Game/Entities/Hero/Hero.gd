extends KinematicBody2D

class_name Hero

signal created_projectile(projectile)
signal died()

var max_health : int = 6
var health : int = max_health
var dead : bool = false

var rng = RandomNumberGenerator.new()
onready var color : String = "blue"

var accel : int = 3000
var speed : Vector2 = Vector2.ZERO
var max_speed : int = 5000
var friction : float = 0.9

var atk_mod : float = 1.0
var dmg_mod : float = 1.0

const KNOCKBACK_FORCE : int = 1000

var gun : Gun_Base

func _physics_process(delta):
	if not dead:
		var move_dir : Vector2 = Vector2.ZERO

		if Input.is_action_pressed("hero_up"):
			move_dir += Vector2.UP

		if Input.is_action_pressed("hero_down"):
			move_dir += Vector2.DOWN

		if Input.is_action_pressed("hero_left"):
			move_dir += Vector2.LEFT

		if Input.is_action_pressed("hero_right"):
			move_dir += Vector2.RIGHT

		move_dir = move_dir.normalized()

		speed += move_dir*accel*delta
		speed = speed.clamped(max_speed)

		speed *= friction

		# warning-ignore:return_value_discarded
		self.move_and_slide(speed)
		if move_dir != Vector2.ZERO:
			$AnimatedSprite.play(color+"_walk")
		else:
			$AnimatedSprite.play(color+"_default")
		if speed > Vector2.ZERO:
			$AnimatedSprite.flip_h = false
		elif speed < Vector2.ZERO:
			$AnimatedSprite.flip_h = true
	else:
		$Sprite.position = Vector2(rand_range(-5, 5), rand_range(-5, 5))

func get_gun(new_gun : Gun_Base) -> Gun_Base:
	var old_gun : Gun_Base = gun
	if old_gun != null :
		if gun.is_in_group("projectile_gun"):
			old_gun.disconnect("created_projectile", self, "_on_gun_created_projectile")
		self.remove_child(old_gun)

	self.add_child(new_gun)
	gun = new_gun

	if gun.is_in_group("projectile_gun"):
		# warning-ignore:return_value_discarded
		gun.connect("created_projectile", self, "_on_gun_created_projectile")

	return old_gun

func _on_gun_created_projectile(projectile):
	if not dead:
		emit_signal("created_projectile", projectile)

func damage(enemy_global_position : Vector2):
	if not dead and $Invulnerable.is_stopped():
		var enemy_dir = (self.global_position - enemy_global_position).normalized()

		self.speed += enemy_dir*KNOCKBACK_FORCE

		health -= 1
		if health == 0:
			dead = true
			$Tween.interpolate_property(self, "scale", self.scale, Vector2.ZERO, 2)
			$Tween.interpolate_property(self, "modulate:a", self.modulate.a, 0, 2)
			$Tween.start()
		else:
			$AnimationPlayer.play("hitted")
			$Invulnerable.start()

func die():
	$AnimatedSprite.play(color+"_die")
	emit_signal("died")

func monster():
	rng.randomize()
	var n = rng.randi_range(0,2)
	match n:
		0:
			$AnimatedSprite.modulate = Color8(64,107,234,255)
			color = "blue"
		1:
			$AnimatedSprite.modulate = Color8(234,223,64,255)
			color = "yellow"
		2:
			$AnimatedSprite.modulate = Color8(73, 186, 67, 255)
			color = "green"
	$Sprite.modulate.a = 0
