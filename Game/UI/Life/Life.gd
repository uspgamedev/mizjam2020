extends Control

var hero : Hero
var hero_max_health : int
export(PackedScene) var heart : PackedScene

func setup(hero_node : Hero):
	hero = hero_node
	hero_max_health = hero.max_health

	for dead_heart in $Hearts.get_children():
		dead_heart.call_deferred("free")

	# warning-ignore:integer_division
	for _ht in range(hero_max_health/2):
		var new_heart = heart.instance()
		new_heart.value = 2
		$Hearts.add_child(new_heart)

	$Panel.rect_size = $Hearts.rect_size

# warning-ignore:unused_argument
func _physics_process(delta):
	if hero_max_health != hero.max_health:
		setup(hero)

	$Panel.rect_size = $Hearts.rect_size

	var health : int = hero.health

	for heart in $Hearts.get_children():

		if health >= 2:
			heart.value = 2
			health -= 2

		elif health == 1:
			heart.value = 1
			health = 0

		else:
			heart.value = 0
