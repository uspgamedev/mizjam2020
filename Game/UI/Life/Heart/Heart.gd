extends Control

var value : int = 0 setget change_value

func change_value(new_val : int):
	assert(new_val == 0 or new_val == 1 or new_val == 2)

	match(new_val):

		0:
			$Heart.frame = 520
			$Heart.modulate = Color(0.278431, 0.278431, 0.278431)
			$Anim.stop()

		1:
			$Heart.frame = 521
			$Heart.modulate = Color(1, 0.341176, 0.341176)
			$Anim.play("loop")

		2:
			$Heart.frame = 522
			$Heart.modulate = Color(1, 0.341176, 0.341176)
			$Anim.play("loop")
