extends Node2D

class_name Gun_Base

onready var gun_sprite : Sprite = $Sprite

func rotate_gun():
	var mouse_pos : Vector2 = get_global_mouse_position()
	self.look_at(mouse_pos)

func _input(event):
	if event.is_action_pressed("hero_shoot"):
		shoot()

func shoot():
	pass
