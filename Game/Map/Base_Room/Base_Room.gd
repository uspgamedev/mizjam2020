extends Node2D

class_name Base_Room

var hero : Hero
var enemy_count : int = 100

signal room_completed()

func _ready():
	yield(get_tree().create_timer(0.2), "timeout")

	enemy_count = $Enemies.get_child_count()

	for enemy in $Enemies.get_children():
		enemy.hero = self.hero
		enemy.connect("died", self, "_enemy_died")

	# warning-ignore:return_value_discarded
	hero.connect("created_projectile", self, "_on_Hero_created_projectile")

func _on_Hero_created_projectile(projectile):
	add_child(projectile)

func _enemy_died():
	enemy_count -= 1
	if enemy_count == 0:
		self.emit_signal("room_completed")

func complete_room():
	self.emit_signal("room_completed")
