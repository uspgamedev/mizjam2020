extends Node2D

func _ready():
	randomize()
	# warning-ignore:return_value_discarded
	$Hero.connect("created_projectile", self, "_on_Hero_created_projectile")
	for thing in self.get_children():
		if thing is Enemy:
			thing.hero = $Hero
			if thing.is_in_group("boss"):
				thing.connect("created_projectile", self, "_on_Hero_created_projectile")

func _on_Hero_created_projectile(projectile):
	add_child(projectile)
