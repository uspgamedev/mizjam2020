extends Popup

var dialogue setget dialogue_set
var space setget space_set

var stop : bool = false

signal closed()

func dialogue_set(new_value):
	dialogue = new_value
	$ColorRect/Dialogue.text = new_value

func space_set(new_value):
	space = new_value
	$ColorRect/Space.text = new_value

func open():
	get_tree().paused = true
	popup()
	$AnimationPlayer.play("ShowDialogue")

func close():
	get_tree().paused = false
	hide()

func _on_AnimationPlayer_animation_finished(anim_name):
	stop = true

func _process(_delta):
	if Input.is_action_pressed("hero_pickup") and stop:
		close()
		emit_signal("closed")
