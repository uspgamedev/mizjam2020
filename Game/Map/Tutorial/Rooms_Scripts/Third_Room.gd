extends Node2D

onready var talk = $Talk_Button
onready var potion_sprite = $NPC/Potion
onready var potion_collision = $NPC/Potion_Collision
onready var dialogue = $CanvasLayer/Dialogue_Popup

var hero : Hero

var dialogue_played : bool = false

signal proceed()
signal back()
signal monsterfy()

func _ready():
	dialogue.connect("closed", self, "_on_closed")

func _input(event):
	if talk.visible and event.is_action_pressed("hero_pickup"):
		dialogue.open()
		talk.hide()

func _on_Area2D_body_entered(body):
	if(body.has_method("move_and_slide")):
		talk.show()

func _on_Area2D_body_exited(body):
	if(body.has_method("move_and_slide")):
		talk.hide()

func _on_closed():
	Tutorial_music.stop()
	InGame_music.play()
	emit_signal("monsterfy")
