extends Enemy

export(float) var speed

var accel : int = 5000
var friction : float = 0.9

var corners : Array = [Vector2(128, 576), Vector2(1152, 576), Vector2(1152, 128), Vector2(128, 128)]
var scape : Array = [6*PI, PI, 0]

var on_area : bool = false
var positive_x : bool = false

func _physics_process(delta):
	var pos = self.global_position
	var target = hero.global_position
	var direction = (pos - target).normalized()
	var movement = direction * speed * delta
	var scaping = scape[0]
	
	movement *= friction
	
	if direction.x < 0:
		positive_x = true
	else: positive_x = false
	
	if on_area:
		$Chicken_Sprite.flip_h = positive_x
		$Mouse_Button.show()
		self.move_and_slide(movement)
	else: $Mouse_Button.hide()


func _on_Notify_Area_body_entered(body):
	if(body.has_method("get_gun")):
		on_area = true

func _on_Notify_Area_body_exited(body):
	if(body.has_method("get_gun")):
		on_area = false
