extends Area2D

signal restore
var on_area : bool

func _ready():
	$CPUParticles2D.emitting = true
	get_parent().get_node("MoreLife").connect("more_life", self, "_pickup")

func _input(event):
	if on_area and event.is_action_pressed("hero_pickup"):
		emit_signal("restore")
		$CPUParticles2D.emitting = false
		self.queue_free()

func _pickup():
	$CPUParticles2D.emitting = false
	self.queue_free()

func _on_Restore_body_entered(body):
	$Pickup_Button.show()
	on_area = true

func _on_Restore_body_exited(body):
	$Pickup_Button.hide()
	on_area = false
