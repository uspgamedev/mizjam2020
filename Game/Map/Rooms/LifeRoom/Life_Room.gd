extends Base_Room

func _ready():
	$Restore.connect("restore", self, "_restore")
	$MoreLife.connect("more_life", self, "_morelife")

func _restore():
	hero.health = hero.max_health
	self.emit_signal("room_completed")
	$Text.visible = false

func _morelife():
	hero.health += 2
	hero.max_health += 2
	self.emit_signal("room_completed")
	$Text.visible = false
