extends Node2D

var rooms : Array = []

var cur_room : Base_Room
var room_list_pointer : int = 0

export(PackedScene) var hero_scene : PackedScene
var player : Hero

var changing : bool = false

func _ready():
	randomize()

	get_tree().paused = false

	$Tween.interpolate_property(	$Panel, "rect_position",
								Vector2.ZERO, OS.window_size/2,
								1.5, Tween.TRANS_ELASTIC)
	$Tween.interpolate_property(	$Panel, "rect_size",
								OS.window_size, Vector2.ZERO,
								1.5, Tween.TRANS_ELASTIC)
	$Tween.start()

	var battle_rooms : Array = []
	var battle_rooms_aux : Array = []
	var gun_rooms : Array = []
	var bonus_rooms : Array = []

	var path : String = ""
	var dir : Directory = Directory.new()
	var total_rooms : int = 0

	#Get Battle Rooms
	path = "res://Map/Rooms/Battle/"
	if dir.open(path) == OK:
		# warning-ignore:return_value_discarded
		dir.list_dir_begin(true, true)
		var file_name : String = dir.get_next()
		while file_name != "":
			total_rooms += 1
			battle_rooms.append( path+file_name )
			file_name = dir.get_next()

	else:
		print("An error occurred when trying to access the battle path.")

	dir.list_dir_end()

	gun_rooms.append("res://Map/Rooms/Random_Guns.tscn")
	bonus_rooms.append("res://Map/Rooms/LifeRoom/Life_Room.tscn")

	#Making room list

	battle_rooms.shuffle()
	gun_rooms.shuffle()

	battle_rooms_aux = battle_rooms.duplicate(true)

	while total_rooms > 0:
		if rooms.size()%6 == 0:
			rooms.append(gun_rooms.back())

		elif rooms.size()%6 == 3:
			rooms.append(bonus_rooms.back())

		else:
			rooms.append(battle_rooms_aux.pop_back())

		if battle_rooms_aux.empty():
			battle_rooms.shuffle()
			battle_rooms_aux = battle_rooms.duplicate(true)

		total_rooms -= 1


	$BG.rect_size = OS.window_size

	player = hero_scene.instance()
	player.position = OS.window_size/2
	player.monster()
	# warning-ignore:return_value_discarded
	player.connect("died", self, "_hero_died")
	$Game.add_child(player)

	$Life.setup(player)

	cur_room = load(rooms[0]).instance()
	cur_room.hero = player
	# warning-ignore:return_value_discarded
	cur_room.connect("room_completed", self, "show_portal")
	$Game.add_child(cur_room)

	$Next_Level_Portal.position = OS.window_size/2
	$Next_Level_Portal.available = false

func show_portal():
	$Next_Level_Portal.available = true

func change_level():
	room_list_pointer += 1

	if room_list_pointer == rooms.size():
		$Game.remove_child($Game.get_node("Hero"))
		player.health = player.max_health
		Global.hero = player
		get_tree().change_scene("res://Map/Final_Room/Final_Room.tscn")
		return

	$Next_Level_Portal.available = false
	get_tree().paused = true
	changing = true
	$Tween.interpolate_property(	$Panel, "rect_position",
								OS.window_size/2, Vector2.ZERO,
								0.5, Tween.TRANS_EXPO)
	$Tween.interpolate_property(	$Panel, "rect_size",
								Vector2.ZERO, OS.window_size,
								0.5, Tween.TRANS_EXPO)
	$Tween.start()

func _on_Tween_tween_all_completed():
	if changing:
		changing = false
		player.position = OS.window_size/2

		$Game.remove_child(cur_room)
		cur_room.call_deferred("free")

		cur_room = load(rooms[room_list_pointer]).instance()
		cur_room.hero = player

		# warning-ignore:return_value_discarded
		cur_room.connect("room_completed", self, "show_portal")
		$Game.add_child(cur_room)

		$Tween.interpolate_property(	$Panel, "rect_position",
								Vector2.ZERO, OS.window_size/2,
								1.5, Tween.TRANS_ELASTIC)
		$Tween.interpolate_property(	$Panel, "rect_size",
								OS.window_size, Vector2.ZERO,
								1.5, Tween.TRANS_ELASTIC)
		$Tween.start()

	else:
		get_tree().paused = false

func _hero_died():
	$Timer.start()

func _on_Timer_timeout():
	get_tree().change_scene("res://Menus/Title_Screen/Main_Menu.tscn")
	InGame_music.stop()
